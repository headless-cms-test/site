import { CONTENT_BASE_URL } from '$lib/config';

export const load = async () => {
	const content = await fetch(`${CONTENT_BASE_URL}index.md`).then((r) => r.text());

	return { content };
};
