import { CONTENT_BASE_URL } from '$lib/config';

export const load = async ({ params }) => {
	const content = await fetch(`${CONTENT_BASE_URL}posts/${params.slug}.md`).then((r) => r.text());

	return { content };
};
