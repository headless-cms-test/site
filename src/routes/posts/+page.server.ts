import { API_BASE_URL } from '$lib/config';

export const load = async () => {
	const posts = await fetch(
		`${API_BASE_URL}repository/tree?path=posts&ref=main&recursive=false`
	).then((r) => r.json());

	return { posts };
};
